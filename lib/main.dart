import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: Padding(
            padding: EdgeInsets.only(left: 12),
            child: IconButton(
              icon: Icon(Icons.menu),
              onPressed: (){
                print('Menu');
              },
            ),
          ),
          title: Row(
            //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Evisd App'),
              ]
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.favorite),
              onPressed: (){
                print('like');
              },
            ),
            IconButton(
              icon: Icon(Icons.more_vert),
              onPressed: (){
                print('Prefences');
              },
            ),
          ],
          backgroundColor: Colors.lightBlue,
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Image.network('https://i.ibb.co/3S9ZRZ3/PSX-20201005-044705.jpg',
                width: 400.0,
                height: 400.0,
                fit: BoxFit.cover,
              ),

              Container(
              alignment: Alignment(0,0.6),
              child: Text(
                'Putu Evi Susita Dewi\n'
                    '1915051098',
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: (){
          },
          child: Icon(Icons.done),
          backgroundColor: Colors.pink,
        ),
      ),
    ),
  );
}